'use strict'

const countChar = (word, char) => (word.split(char)).length
const isOnBasement = (acu, cur, index, arr) => {
  acu += cur === ')' ? -1 : 1
  if (acu === -1) {
    acu = index + 1
    arr.splice(1)
  }
  return acu
}

module.exports = {
  part1: input => countChar(input, '(') - countChar(input, ')'),
  part2: input => [...input].reduce(isOnBasement, 0)
}
