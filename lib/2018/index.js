'use strict'

module.exports = {
  ex1: require('./01'),
  ex2: require('./02'),
  ex3: require('./03'),
  ex4: require('./04'),
  ex5: require('./05'),
  ex6: require('./06'),
  ex7: require('./07'),
  ex8: require('./08'),
  ex9: require('./09'),
  ex10: require('./10'),
  ex11: require('./11'),
  ex12: require('./12'),
  ex13: require('./13'),
  ex14: require('./14'),
  ex15: require('./15'),
  ex16: require('./16'),
  ex17: require('./17'),
  ex18: require('./18'),
  ex19: require('./19'),
  ex20: require('./20'),
  ex21: require('./21'),
  ex22: require('./22'),
  ex23: require('./23'),
  ex24: require('./24'),
  ex25: require('./25')
}
