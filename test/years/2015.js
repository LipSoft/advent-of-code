'use strict'
/* global describe, it, before */

const expect = require('chai').expect
const adventOfCode = require('../../lib/2015')
const fs = require('fs')
const path = require('path')

const input1 = fs.readFileSync(path.join('Build', '2015', 'input1.txt'), 'utf-8')
const input2 = fs.readFileSync(path.join('Build', '2015', 'input2.txt'), 'utf-8')

describe('#2015 Advent of code', function () {
  before(() => {

  })
  describe('#Exercise 0', function () {
    it('Should return \'\' with input: input ', () => expect('').to.be.equal(''))
    it('Should return \'\' with input: input  ', () => expect('').to.be.equal(''))
    it('Should return ??? with challange input', () => expect('').to.be.equal(''))
  })

  describe('#Exercise 1', function () {
    it('Should return 0 with input: (()) ', () => expect(adventOfCode.ex1.part1('(())')).to.be.equal(0))
    it('Should return 0 with input: ()() ', () => expect(adventOfCode.ex1.part1('()()')).to.be.equal(0))
    it('Should return 3 with input: ((( ', () => expect(adventOfCode.ex1.part1('(((')).to.be.equal(3))
    it('Should return 3 with input: (()(()( ', () => expect(adventOfCode.ex1.part1('(()(()(')).to.be.equal(3))
    it('Should return 3 with input: ))((((( ', () => expect(adventOfCode.ex1.part1('))(((((')).to.be.equal(3))
    it('Should return -1 with input: ()) ', () => expect(adventOfCode.ex1.part1('())')).to.be.equal(-1))
    it('Should return -1 with input: ))( ', () => expect(adventOfCode.ex1.part1('))(')).to.be.equal(-1))
    it('Should return -3 with input: ))) ', () => expect(adventOfCode.ex1.part1(')))')).to.be.equal(-3))
    it('Should return -3 with input: )())()) ', () => expect(adventOfCode.ex1.part1(')())())')).to.be.equal(-3))
    it('Should return 232 with challange input', () => expect(adventOfCode.ex1.part1(input1)).to.be.equal(232))

    it('Should return 1 with input: )', () => expect(adventOfCode.ex1.part2(')')).to.be.equal(1))
    it('Should return 5 with input: ()())', () => expect(adventOfCode.ex1.part2('()())')).to.be.equal(5))
    it('Should return 1783 with challange input', () => expect(adventOfCode.ex1.part2(input1)).to.be.equal(1783))
  })

  describe('#Exercise 2', function () {
    it('Should return 58 with input: 2x3x4', () => expect(adventOfCode.ex2.part1('2x3x4')).to.be.equal(58))
    it('Should return 43 with input: 1x1x10', () => expect(adventOfCode.ex2.part1('1x1x10')).to.be.equal(43))
    it('Should return 1606483 with challange input', () => expect(adventOfCode.ex2.part1(input2)).to.be.equal(1606483))

    it('Should return 34 with input: 2x3x4', () => expect(adventOfCode.ex2.part2('2x3x4')).to.be.equal(34))
    it('Should return 14 with input: 1x1x10', () => expect(adventOfCode.ex2.part2('1x1x10')).to.be.equal(14))
    it('Should return 3842356 with challange input', () => expect(adventOfCode.ex2.part2(input2)).to.be.equal(3842356))
  })
})
