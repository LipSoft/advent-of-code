'use strict'
/* global describe, it, before */

const expect = require('chai').expect
const adventOfCode = require('../../lib/2016')

describe('#2015 Advent of code', function () {
  before(() => {

  })
  describe('#Exercise 0', function () {
    it('Should return \'\' with a good config ', () => expect('').to.be.equal(''))
    it('Should return \'\' with a good config ', () => expect('').to.be.equal(''))
  })

  describe('#Exercise 1', function () {
    it('Should return \'\' with a good config ', () => expect(adventOfCode.ex1.part1()).to.be.equal(''))
    it('Should return \'\' with a good config ', () => expect(adventOfCode.ex1.part2()).to.be.equal(''))
  })
})
