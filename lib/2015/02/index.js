'use strict'

const prepareInput = input => input.split('\r\n')
const calculatePapper = (acu, cur) => {
  const sides = cur.split('x')
  const smallest = Math.min(sides[0] * sides[1], sides[1] * sides[2], sides[0] * sides[2])
  return acu + (2 * sides[0] * sides[1]) + (2 * sides[1] * sides[2]) + (2 * sides[0] * sides[2]) + smallest
}

const calculateRibbon = (acu, cur) => {
  const sides = cur.split('x').map(x => +x)
  const biggest = Math.max(...sides)
  sides.splice(sides.indexOf(biggest), 1)
  return acu + (2 * sides[0]) + (2 * sides[1]) + (sides.reduce((acu, cur) => acu * cur, 1) * biggest)
}

module.exports = {
  part1: input => prepareInput(input).reduce(calculatePapper, 0),
  part2: input => prepareInput(input).reduce(calculateRibbon, 0)
}
